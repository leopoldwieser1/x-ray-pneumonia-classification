import numpy as np
import os
import PIL
import PIL.Image
import tensorflow as tf
#import tensorflow_datasets as tfds
from tensorflow.keras.preprocessing.image import ImageDataGenerator

import sys


workdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(workdir)

data_dir = os.path.join(workdir, 'data/chest_xray')
# AUTOTUNE = tf.data.AUTOTUNE
normalization_layer = tf.keras.layers.experimental.preprocessing.Rescaling(1. / 255)


# define the type of augmentation techniques we will apply.
def to_grayscale_then_rgb(image):
    image = tf.image.rgb_to_grayscale(image)
    image = tf.image.grayscale_to_rgb(image)
    return image
train_Datagen = ImageDataGenerator(
    rescale=1/255,
    shear_range=10,
    zoom_range=0.2,
    horizontal_flip=True,
    width_shift_range=0.2,
        height_shift_range=0.2,
        rotation_range=20,
    fill_mode='nearest',
    preprocessing_function=to_grayscale_then_rgb,
)
val_datagen = ImageDataGenerator(
    rescale=1/255,
    preprocessing_function=to_grayscale_then_rgb,
)



class DataLoader:
    def __init__(self, path_to_picture_directory, batch_size=16, shuffle_buffer_size=1024, img_width=500,
                 img_height=500):
        self.path_to_picture_directory = path_to_picture_directory
        self.img_width = img_width
        self.img_height = img_height
        self.data = {}

        self.train_generator = train_Datagen.flow_from_directory(
            os.path.join(data_dir, 'train'),
            target_size=(img_height, img_width),
            batch_size=32,
            class_mode='binary'
        )
        self.validation_generator = val_datagen.flow_from_directory(
            os.path.join(data_dir, 'val'),
            target_size=(img_height, img_width),
            batch_size=8,
            class_mode='binary'
        )
        self.test_generator = val_datagen.flow_from_directory(
            os.path.join(data_dir, 'test'),
            target_size=(img_height, img_width),
            batch_size=16,
            class_mode='binary'
        )



