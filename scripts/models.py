import tensorflow as tf

from tensorflow.keras import layers



# Auxiliary functions: currently unused

def dense_layer(prev_layer, avoid_overfitting_by, n_labels, dropout_factor=0):
    x = layers.Dense(int((2 / 3) * prev_layer.shape[1] + n_labels), activation='relu')(prev_layer)
    if 'batch_normalization' in avoid_overfitting_by and 'dropout' in avoid_overfitting_by:
        x = layers.BatchNormalization()(x)
        x = layers.Dropout(dropout_factor)(x)
        print(f'using batch normalization and dropout {dropout_factor}')
    elif 'batch_normalization' in avoid_overfitting_by:
        x = layers.BatchNormalization()(x)
        print(f'using batch normalization')
    elif 'dropout' in avoid_overfitting_by:
        x = layers.Dropout(dropout_factor)(x)
        print(f'using dropout {dropout_factor}')
    else:
        print('no overfitting avoidance')
    return x



# Image Models

def resnet_model(run, img_input_shape=(500, 500, 3), n_labels=1):
    inputs = layers.Input(shape=img_input_shape)
    resnet = tf.keras.applications.resnet50.ResNet50(
        include_top=False, weights='imagenet', input_tensor=inputs,
        input_shape=img_input_shape, pooling='avg')
    for i in resnet.layers:
        i.trainable = False
    x = resnet(inputs)
    x = layers.Dense(run['layer_sizes'][0], activation='relu')(x)
    x = layers.Dropout(run['dropout_factor'])(x)
    x = layers.Dense(run['layer_sizes'][1], activation='relu')(x)
    x = layers.Dropout(run['dropout_factor'])(x)
    x = layers.Dense(run['layer_sizes'][2], activation='relu')(x)
    x = layers.Dropout(run['dropout_factor'])(x)

    output = tf.keras.layers.Dense(n_labels, activation='sigmoid', name='output')(x)
    model = tf.keras.Model(inputs=[inputs], outputs=[output])

    print(model.summary())
    return model


def vgg_model(run, img_input_shape=(500, 500, 3), n_labels=1):
    inputs = layers.Input(shape=img_input_shape)
    vgg16 = tf.keras.applications.vgg16.VGG16(
        include_top=False, weights='imagenet', input_tensor=inputs,
        input_shape=img_input_shape, pooling='avg')
    for i in vgg16.layers:
        i.trainable = False
    x = vgg16(inputs)
    x = layers.Dense(run['layer_sizes'][0], activation='relu')(x)
    x = layers.Dropout(run['dropout_factor'])(x)
    x = layers.Dense(run['layer_sizes'][1], activation='relu')(x)
    x = layers.Dropout(run['dropout_factor'])(x)
    x = layers.Dense(run['layer_sizes'][2], activation='relu')(x)
    x = layers.Dropout(run['dropout_factor'])(x)
    output = tf.keras.layers.Dense(n_labels, activation='sigmoid', name='output')(x)
    model = tf.keras.Model(inputs=[inputs], outputs=[output])

    print(model.summary())
    return model
