import matplotlib.pyplot as plt
import sys
import os
import numpy as np
workdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(workdir)
# file unused


def sample_output_image(model, ds, categories, save_path, n=9, threshold= 0.3):
    y_data = ds.batch(batch_size=n).take(1)
    predictions= model.predict(y_data)
    print("A sample output from the last layer (model) ", predictions[0])
    print(f"{n} Sample predictions:")
    fig = plt.figure(figsize=(20, 20))
    columns = 3
    rows = 3
    print(columns * rows, "samples from the dataset")
    i = 1
    for (pred, (a, c)) in zip(predictions, ds.take(10)):
        pred[pred > threshold] = 1
        pred[pred <= threshold] = 0
        predicted = ', '.join(covert_onehot_string_labels(categories, pred))
        actual = ', '.join(covert_onehot_string_labels(categories, c.numpy()))
        info_string = f"Predicted genres: {predicted}\n  Actual genres: {actual}"
        print(info_string)
        if i <= 9:
            fig.add_subplot(rows, columns, i)
            plt.imshow(np.squeeze(a))
            plt.title(info_string)
            i = i + 1
    plt.subplots_adjust(left=0.125,
                        bottom=0.1,
                        right=0.9,
                        top=0.9,
                        wspace=0.5,
                        hspace=0.5)
    plt.savefig(save_path)
    plt.show()
