import json
import sys
import os
import numpy as np
import tensorflow as tf
import random

workdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, workdir)

from data_loader import DataLoader
from models import resnet_model, vgg_model
from save_results import *

SEED = 0


def set_seeds(seed=SEED):
    os.environ['PYTHONHASHSEED'] = str(seed)
    random.seed(seed)
    tf.random.set_seed(seed)
    np.random.seed(seed)


def set_global_determinism(seed=SEED):
    set_seeds(seed=seed)
    os.environ['TF_DETERMINISTIC_OPS'] = '1'
    os.environ['TF_CUDNN_DETERMINISTIC'] = '1'
    tf.config.threading.set_inter_op_parallelism_threads(1)
    tf.config.threading.set_intra_op_parallelism_threads(1)


set_global_determinism(seed=SEED)

data_dir = os.path.join(workdir, 'data/chest_xray')

with open(os.path.join(workdir, 'run_templates/Run.json')) as f:
    runs = json.load(f)

for run_dict in runs:
    for seed in range(run_dict['iterations']):
        set_seeds(seed)

        tf.keras.backend.clear_session()
        img_height = 150
        img_width = 150
        data = DataLoader(data_dir, img_height=img_height, img_width=img_width)
        if run_dict['class_weights']:
            n = data.train_generator.samples
            n_0 = n - sum(data.train_generator.labels)
            n_1 = sum(data.train_generator.labels)
            weight_for_0 = (1 / n_0) * (n / 2.0)
            weight_for_1 = (1 / n_1) * (n / 2.0)
            class_weight = {0: weight_for_0, 1: weight_for_1}
        else:
            class_weight = {0: 1, 1: 1}
        if run_dict['name'] == 'resnet_model':
            model = resnet_model(run_dict, img_input_shape=(img_height, img_width, 3))
        elif run_dict['name'] == 'vgg_model':
            model = vgg_model(run_dict, img_input_shape=(img_height, img_width, 3))
        else:
            raise ValueError('Use different model')

        loss = tf.keras.losses.binary_crossentropy
        optimizer = tf.keras.optimizers.Adam(lr=run_dict['learning_rate'])
        metrics = [tf.keras.metrics.BinaryAccuracy(name='test_accuracy'),
                   tf.keras.metrics.Recall(name='recall'),
                   tf.keras.metrics.Precision(name='precision'),
                   tf.keras.metrics.FalseNegatives(name='fn'),
                   tf.keras.metrics.FalsePositives(name='fp'),
                   tf.keras.metrics.TrueNegatives(name='tn'),
                   tf.keras.metrics.TruePositives(name='tp')]
        model.compile(
            optimizer=optimizer,
            loss=loss,
            metrics=metrics)
        log_dir = os.path.join(workdir,
                               "log_v2/fit/" + run_dict['name'] + f"/{'_'.join([str(x) for x in run_dict.values()])}")

        tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir)
        early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=4, min_delta=1e-3,
                                                          restore_best_weights=True)

        history = model.fit(data.train_generator, epochs=run_dict['epochs'], validation_data=data.validation_generator,
                            callbacks=[tensorboard_callback, early_stopping], verbose=2, class_weight=class_weight)

        evaluate(run_dict, model, data.test_generator, history)
        for layer in model.layers[1]:
            layer.trainable = True

        model.compile(
            optimizer=tf.keras.optimizers.Adam(lr=run_dict['learning_rate'] / 10),
            loss=loss,
            metrics=metrics)
        history = model.fit(data.train_generator, epochs=run_dict['epochs'], validation_data=data.validation_generator,
                            callbacks=[tensorboard_callback, early_stopping], verbose=2, class_weight=class_weight)
        evaluate(run_dict, model, data.test_generator, history)
