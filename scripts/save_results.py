import os
import sys

import pandas as pd

workdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(workdir)


def summarize(run, hist, test_eval):
    col_dict = {'name': run['name'],
                'epochs': len(hist.history['loss']),
                'initial_learning_rate': run['learning_rate'],
                'layer_sizes': str(run['layer_sizes']),
                'dropout_factor': run['dropout_factor']}
    col_dict.update(test_eval)
    col_dict['f1_score'] = 2 * (col_dict['precision'] * col_dict['recall']) / (
            col_dict['precision'] + col_dict['recall'])
    col = pd.Series(col_dict)
    return col


def evaluate(run, model, test_data, hist, path=os.path.join(workdir, 'runs/run_results_v5.csv')):
    try:
        df = pd.read_csv(path)
    except FileNotFoundError:
        df = pd.DataFrame()
    test_eval = model.evaluate(test_data, return_dict=True)
    col = summarize(run, hist, test_eval)
    df1 = pd.concat([df, pd.DataFrame(col).T], axis=0, ignore_index=True)
    df1.to_csv(path, index=False)
