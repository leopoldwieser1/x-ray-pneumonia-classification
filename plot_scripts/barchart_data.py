from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import rc

import os
import sys
workdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(workdir)

rc('font', **{'family': 'serif', 'sans-serif': ['Libertine']})
rc('text', usetex=True)
plt.rc('xtick', labelsize=10)
plt.rc('ytick', labelsize=10)
plt.rc('axes', labelsize=10)
width = 3.487
height = width / 1.618

fig = plt.figure(dpi=400, figsize=(width, height))
ax = fig.add_subplot(111)


X = ["Train", "Test"]
with_pnemonia = [3870, 390]
without_pneumonia = [1340, 234]

X_axis = np.arange(len(X))

ax.bar(X_axis - 0.2, with_pnemonia, 0.4, label='With Pneumonia')
ax.bar(X_axis + 0.2, without_pneumonia, 0.4, label='Without Pneumonia')

plt.xticks(X_axis, X)
plt.xlabel("Data Set")
plt.ylabel("Number of Samples")
plt.title("Data Set Composition")
plt.legend()
plt.savefig(os.path.join(workdir, "plots/data_set_composition.png"))
plt.show()


