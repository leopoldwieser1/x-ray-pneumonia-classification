import pandas
import pandas as pd
import os
import seaborn as sns
import matplotlib.pyplot as plt


data_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "runs", "run_results_v5.csv")
print(data_path)
df = pd.read_csv(data_path)
df_vgg = df.loc[range(26, 36)]
df_resnet = df.loc[range(16, 26)]
dfs = [df_resnet["test_accuracy"], df_vgg["test_accuracy"]]
print(df_vgg)

fig, axs = plt.subplots()
sns.boxplot(data=dfs)
# sns.boxplot(x=df_vgg["precision"], ax=axs)
# sns.boxplot(x=df_resnet["precision"], ax=axs)
# ax.boxplot()
plt.tight_layout()
plt.savefig()
plt.show()
# fig = px.box(df["precision"], y="precision")
# fig.show()
# fig.write_image("boxplot.png")
