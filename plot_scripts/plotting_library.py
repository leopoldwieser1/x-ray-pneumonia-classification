from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sn
import os
import sys

workdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(workdir)

from matplotlib import rc

rc('font', **{'family': 'serif', 'sans-serif': ['Libertine']})
rc('text', usetex=True)
plt.rc('xtick', labelsize=10)
plt.rc('ytick', labelsize=10)
plt.rc('axes', labelsize=10)
width = 3.487
height = width / 1.618


def plot_confusion_matrix(tp, fp, tn, fn):
    m = [[tp, fp], [fn, tn]]

    df_cm = pd.DataFrame(m, index=['Pneumonia', 'Normal'],
                         columns=['Pred. Pneumonia', 'Pred. Normal'])
    plt.figure(figsize=(width, height), dpi=400)
    sn.heatmap(df_cm, annot=True, fmt='d', cmap='plasma_r')
    plt.tight_layout()
    plt.savefig(os.path.join(workdir, 'plots/confusion_matrix.png'))
    plt.show()


def plot_bar_plot():
    fig = plt.figure(dpi=400, figsize=(width, height))
    ax = fig.add_subplot(111)
    X = ["Train", "Test"]
    with_pnemonia = [3870, 390]
    without_pneumonia = [1340, 234]

    X_axis = np.arange(len(X))

    ax.bar(X_axis - 0.2, with_pnemonia, 0.4, label='With Pneumonia')
    ax.bar(X_axis + 0.2, without_pneumonia, 0.4, label='Without Pneumonia')

    plt.xticks(X_axis, X)
    plt.xlabel("Data Set")
    plt.ylabel("Number of Samples")
    plt.legend()
    plt.tight_layout()
    plt.savefig(os.path.join(workdir, "plots/data_set_composition.png"))
    plt.show()


def plot_boxplot():
    data_path = os.path.join(workdir, "runs", "run_results_v5.csv")
    df = pd.read_csv(data_path)
    df_vgg = df.loc[range(26, 36)]
    df_resnet = df.loc[range(16, 26)]
    dfs = [df_resnet["test_accuracy"], df_vgg["test_accuracy"]]

    fig = plt.figure(dpi=400, figsize=(width, height))
    ax = fig.add_subplot(111)
    ax.boxplot(dfs,
               vert=True,  # vertical box alignment
               patch_artist=True,  # fill with color
               labels=['RESNET 50', 'VGG 16'])
    ax.set_ylabel('accuracy')
    plt.tight_layout()
    plt.savefig(os.path.join(workdir, 'plots/boxplot.png'))
    plt.show()


def plot_dropout_rates():
    data_path = os.path.join(workdir, "runs", "run_results_v5.csv")
    df = pd.read_csv(data_path)
    df_vgg = df.loc[range(12, 16)]
    df_resnet = df.loc[range(8, 12)]
    fig = plt.figure(dpi=400, figsize=(width, height))
    ax = fig.add_subplot(111)
    ax.plot([0, 0.2, 0.4, 0.6], df_resnet['test_accuracy'], label='RESNET 50')
    ax.plot([0, 0.2, 0.4, 0.6], df_vgg['test_accuracy'], label='VGG 16')
    ax.set_ylabel('accuracy')
    ax.set_xlabel('dropout rate')
    plt.legend()
    plt.tight_layout()
    plt.savefig(os.path.join(workdir, 'plots/dropout_rate.png'))
    plt.show()

#plot_confusion_matrix(384, 41, 193, 6)
#plot_boxplot()
#plot_dropout_rates()
