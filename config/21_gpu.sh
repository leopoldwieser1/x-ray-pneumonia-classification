#!/bin/bash

#SBATCH --partition=gpucloud
#SBATCH --ntasks=1              # Requested (MPI)tasks. Default=1
#SBATCH --cpus-per-task=128       # Requested CPUs per task. Default=1
#SBATCH --mem=64G               # Memory limit. [1-999][K|M|G|T]
#SBATCH --time=4:00            # Time limit. [[days-]hh:]mm[:ss]
#SBATCH --gpu=2                 # Number of GPUs to use

### configure file to store console output.
### Write output to /dev/null to discard output
#SBATCH --output=pneumonia_class.log

### configure email notifications
### mail types: BEGIN,END,FAIL,TIME_LIMIT,TIME_LIMIT_90,TIME_LIMIT_80
#SBATCH --mail-user=ge36not@mytum.de
#SBATCH --mail-type=BEGIN,END,FAIL,TIME_LIMIT

### give your job a name (and maybe a comment) to find it in the queue
#SBATCH --job-name=pneumonia_class
#SBATCH --comment="gpu example script"

### load environment modules
#module purge
module load spack_skylake_avx512
module load python/3.8-cuda-ml
#module load python-cuda-ml

### set environment variables
export OMP_NUM_THREADS=${SLURM_CPUS_PER_TASK}

### run your program...
srun python ../scripts/model_runner.py

